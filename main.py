import sys

print("""Wilkommen zu dp der Deutschen Programmiersprache. Dateien als .dp
      abspeichern dann dieses Skript ausführen.""")

# Lese Befehlszeilenoptionen

datei: str = None

if len(sys.argv) > 1: # sys.argv: [main.py, datei.dp]
    datei = sys.argv[1]

if datei is None:
    datei = input("Datei: ")

# Funktionen

def typee(a):
    if a.startswith('"'):
        return a.replace('"','')
    if a.startswith("'"):
        return a.replace("'","")
    else:
        print("Error")
        sys.exit(0)
# Auslesen
d = open(datei)

for z in d:
    if z.startswith("ausgeben"):
        merken = z.replace(")","").replace("ausgeben(", "")
        print(typee(merken))
